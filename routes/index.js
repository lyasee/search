var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  let json = {
    title: "검색어 테스트",
    navbarbrand: "상민 인기검색어 v0.0.1"
  }
  res.render('index', json);
});

module.exports = router;
