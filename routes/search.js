const express = require('express');
const router = express.Router();

const Search = require('../models/search');

/* GET home page. */
router.post('/', function(req, res, next) {

    let search = {
        sch_name: req.body.sch_name
    }

    Search.insertSearch(search, (err, success) => {
        if(success){
            res.render('search/result', { result: success })
        }else{
            res.render('search/result', { result: "오류" })
        }
    })

    // res.render('search/result', { result: body.sch_name })
    // console.log('result: ', body.sch_name);

});

router.get('/', (req, res, next) => {

    Search.selectSearch((err, list) => {
        if(list)
            res.render('search/list', { rows: list })
        else
            res.render('search/list', { rows: null })
    })

})

module.exports = router;
