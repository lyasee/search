/*
 * @Author: Sangmin.Lee 
 * @Date: 2017-08-10 21:29:05 
 * @Last Modified by: Sangmin.Lee
 * @Last Modified time: 2017-08-11 01:42:34
 */
const pool = require('../config/database');
const Promise = require('promise');

const mod = require('korean-text-analytics');
const task = new mod.TaskQueue();

/**
 * 생성 - 2017-08-11 이상민, []
 * 검색어 저장 v0.0.1
 */
module.exports.insertSearch = (search, callback) => {

    let textMorph = (text, search) => {

        return new Promise((resolve, reject) => {

            // korean-text-analytics
            // https://github.com/hyunwoo/KoreanTextAnalytics
            // 형태소 분리
            mod.ExecuteMorphModule(text, (err, rep) => {
                if(err) throw err
                if(rep){
                    console.log('rep: ', rep);
                    resolve(rep)
                }
            })

        })
        .then((rep) => {
            return new Promise((resolve, reject) => {

                // 꼬꼬마 형태소 분석기 태그표
                // http://kkma.snu.ac.kr/documents/?doc=postag
                
                let json = { text:[] }
                let text = ""
                for(let i in rep.morphed){ // 형태소 조합
                    let tag = String(rep.morphed[i].tag)
                    if(tag == "NNP" || tag == "VV"){
                        text = text + rep.morphed[i].word
                    }
                    else if(tag == "JKG" || tag == "XSN" || tag == "JKB" || tag == "JX" || tag == "JKS"){
                        text = text + rep.morphed[i].word + ""
                    }
                    else if(tag == "JX" || tag == "EC" || tag == "ETM"){
                        text = text + rep.morphed[i].word + " "
                    }
                    else if(tag == "NNG" || tag == "NP" || tag == "NR"){
                        text = text + " " + rep.morphed[i].word
                    }
                    else if(tag == "SL"){
                        text = text + rep.morphed[i].word + " "
                    }
                    else text = text
                }

                let txt
                // 앞, 뒤 공백 잘라내기
                for(let i=0; i<2; i++){
                    if(text.substring(0, 1) == " "){ // 앞 공백
                        txt = text.substring(1, text.length)
                    }
                    else if(text.substring(text.length-1, text.length) == " "){ // 뒷 공백
                        txt = text.substring(1, text.length)
                    }
                    else txt = text
                }

                resolve(txt)
                
            })
        })
        .then((text) => {
            console.log('text: ', text);
            return new Promise((resolve, reject) => {
                let sql = "INSERT INTO search(sch_name, mem_id, sch_datetime, sch_cnt) VALUES(?, ?, ?, ?)"
                let datas = [text, 175, new Date(), 1]

                // let sql = "", datas = []

                // for(let i in text){
                //     sql = sql + "INSERT INTO search(sch_name, mem_id, sch_datetime, sch_cnt) VALUES(?, ?, ?, ?);"
                //     datas.push(text[i], 175, new Date(), 1)
                // }

                // console.log('sql: ', sql);
                // console.log('datas: ', datas);

                pool.query(sql, datas, (err, rows) => {
                    if(err) throw err
                    if(rows.insertId > 0 || rows[0].insertId > 0){
                        resolve(text)
                    }else{
                        resolve(true)
                    }
                })
                // resolve(true)
            })
        })

    } // textMorph

    textMorph(search.sch_name, search)
    .then(result => {
        callback(null, result)
    })

}


module.exports.selectSearch = (callback) => {

    let sql = "SELECT sch_name, COUNT(sch_name) AS cnt FROM search GROUP BY sch_name HAVING COUNT(*)>1 ORDER BY cnt DESC LIMIT 5;"

    pool.query(sql, (err, rows) => {
        if(err) throw err
        if(rows.length > 0){
            callback(null, rows)
        }else{
            callback(null, null)
        }
    })
}