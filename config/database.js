var mysql = require('mysql');
var config = require('./config');

module.exports = mysql.createPool({
  connectionLimit : config.database.poolLimit,
  host     : config.database.host,
  user     : config.database.user,
  password : config.database.password,
  database : config.database.schema,
  multipleStatements : true
});